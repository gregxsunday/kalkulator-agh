#include "agh.h"

Agh_counter::Agh_counter(QString m_b, QString m_e, QString l_b, QString l_e, bool val_type) :
    main_basic(convert_string(m_b)),
    main_extended(convert_string(m_e)),
    lang_basic(convert_string(l_b)),
    lang_extended(convert_string(l_e)),
    ok(true)
{
    if(main_basic == -1 || main_extended == -1 || lang_basic == -1 || lang_extended == -1)
        ok = 0;
    else if (val_type)
        count_points();
    else if (main_basic > 50 || main_extended > 50 || lang_basic > 50 || lang_extended > 50)
        ok = 0;
    else
    {
        main_basic *= 2;
        main_extended *= 2;
        lang_basic *= 2;
        lang_extended *=2;
        count_points();
    }
}

int Agh_counter::convert_string(QString given)
{
    bool is_ok = 1;
    int value = given.toInt(&is_ok);
    if(value < 0 || value > 100 || !is_ok)
        return -1;
    else return value;
}

void Agh_counter::count_points()
{
    int sum = main_basic + main_extended;
    int g;
    if(main_extended < 30)
        g = main_extended;
    else if(main_extended <= 80)
        g = main_extended + 2*(main_extended - 30);
    else
        g = main_extended + 100;

    g = (g > sum) ? g : sum;
    g *= 4;

    points = g + lang_basic + lang_extended;
}
