#ifndef AGH_H
#define AGH_H

#include <QString>

class Agh_counter
{
    int main_basic,
        main_extended,
        lang_basic,
        lang_extended,
        points;
    bool ok;
public:
    Agh_counter(QString m_b, QString m_e, QString l_b, QString l_e, bool val_type);

    int convert_string(QString given);

    void count_points();

    int get_points()
    {
        return points;
    }

    bool get_ok()
    {
        return ok;
    }

};

#endif // AGH_H
