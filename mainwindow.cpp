#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "agh.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_count_clicked()
{
    bool values_as_precentage = (ui->percentage->isChecked()) ? 1 : 0;

    Agh_counter agh_points(ui->m_b->text(), ui->m_e->text(), ui->l_b->text(), ui->l_e->text(), values_as_precentage);
    bool ok = agh_points.get_ok();
    if(ok)
    {
        int res = agh_points.get_points();
        QString res_str = QString::number(res);
        ui->result->setTextFormat(Qt::RichText);
        ui->result->setText(res_str);
    }
    else
        QMessageBox::warning(this, "Błąd", "Podano niepoprawne wartości");
}

void MainWindow::on_m_e_textEdited(const QString &arg1)
{
    QString val_str = arg1;
    int val = val_str.toInt();
    if(val >= 80)
    {
        ui->m_b->setText("100");
        ui->m_b->setDisabled(1);
    }
    else
    {
        ui->m_b->setText("");
        ui->m_b->setDisabled(0);
    }
}
